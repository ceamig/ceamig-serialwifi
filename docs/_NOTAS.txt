PARA fazer o UPLOAD do programa no ESP8266:

1) conectar o de USB/Serial no conector USB programming (J3)
2) Colocar um jumper no conector J2 (Run/Prog), na posicao PROG (pinos 2 e 3)
3) ligar a placa do ESP, pois os pinos de tensao do programador nao estao ligados ao ESP
4) pressionar e segurar por dois segundos o botao de reset.

--> para verificar se entrou em modo de programacao:
- O led vermelho do ESP tem que estar ligado
- o led azul do ESP tem que estar apagado

Configuracao de Programacao no arduino IDE:
- porta Serial (variavel, cada porta USB do computador dá origem a uma porta serial diferente)
- Upload Speed: 115200 (256000 costuma dar erro)
- CPU Frequency: 80 MHz (nao testei, mas deve funcionar em 160 MHz)
- Flash Mode: QIO (um pouco mais rapido). Funciona com DIO tb.
- Flash Frequency: 40 MHz 
- Flash Frequency em 80 MHz funciona (tem que dar um reset antes de cada ciclo de programacao, senao da' erro na prog) 
- prog: cerca de 30 seg



