 // Ceamig-SerialWifi - Ceamig Access Point Serial-Wifi
//
// ESP8266 Boot mode:
//                 GPIO15 GPIO0 GPIO2    (boot mode - decimal)
// upload/UART       0      0     1         1
// normal boot       0      1     1         3
// SD-Card boot      1      0     0         4     (nao usado)

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "FS.h"
#include <EEPROM.h>
#include <String.h>
#include "Util.h"
#include "ConfigHw.h"

int G_debug = 0;

const char VERSAO[] = "1.0.0";

// definicoes WiFi:
const char WiFiSSID[]     = "CEAMIG";
const int  WiFiPort       = 4030;			// porta de interface - para Meade
const int  WiFiPortConfig = 80;				// porta para configuracoes (para web browser)

WiFiServer server(WiFiPort);					// server de interface com o telescopio
ESP8266WebServer serverConfig(WiFiPortConfig);        // server de configuracao de parametros do telescopio


//----------- Estruturas de configuracao para gravar em EEPROM
// Preferivel usar EEPROM pois nao sobrescreve os dados de configuracao quando grava o SSPI.
const char cfgAssinatura[5] = "0607";       // Maximo: 4 bytes
// Assinatura para garantir que as estruturas de configuracao estao corretas e foram inicializadas.
const unsigned int ADDR_EEPROM_SIGN = 0;
const unsigned int ADDR_EEPROM_CONFIGHW = ADDR_EEPROM_SIGN + sizeof(cfgAssinatura);

// Parametros defaut de configuracao: (podem ser mudados pela pagina de configuracao web)
struct ConfigHw_st  configHw  =  { 
		"Ceamig-SerialWiFi",
		IPAddress(10,0,0,1),
		IPAddress(255,255,255,0),
		4030,
		9600,
		SERIAL_8N1,
	};

const unsigned int EEPROM_SIZE = sizeof(cfgAssinatura) + sizeof(configHw);
//---------------------------------------


void setup()
{
  initHardware();
  delay(500);

  //Serial.println();

  Serial.println("Configurando Ponto de Acesso...");

  EEPROM.begin(EEPROM_SIZE);

  SPIFFS.begin();  // inicializa sistema de arquivos

  // Verifica assinatura na configuracao da EEPROM e carrega os dados armazenados:
  if (VerificarAssinatura()) {
    //GravarParametrosHw(); // Serial.println("Gravar OK");// executar somente para gravar inicialmente os parametros
    // teste:
    //configHw.latitude = 33.7;

    // Assinatura Ok. Ler parametros:
    LerParametrosHw();
  } else {
    // Assinatura incorreta. Gravar parametros default:
    GravarAssinatura();
    GravarParametrosHw();
    // Le novamente para ter parametros validos na var cfg:
    LerParametrosHw();
    //Serial.print("Erro Assinatura EEPROM\r\n");
    //tone(300, 500);  // erro!
  }

  //Serial.println("Configurando Ponto de Acesso...");
  configWiFi(&configHw, true);

  //SPIFFS.begin();  // inicializa sistema de arquivos

  
  //if (0) {
  //  // para debug - verificar a memoria flash e mostrar na Serial:
  //  checkFlash();
  //}

  // LER DA CONFIGURACAO
  //Serial.println("");
  //Serial.println("-- Config: --");
  //Serial.println("-------------");

  Serial.println("Fim configuracao.");

  delay(100);
}
//--------------------------------------------------------------

int GravarParametrosHw()
{
  //for (unsigned int i = 0; i < sizeof(configHw_st); i++)
  //	EEPROM.write(ADDR_EEPROM_CONFIGHW + i, *((char*)&configHw + i));
  return writeToEEPROM(ADDR_EEPROM_CONFIGHW, configHw);
}
// ---------------------------------------------------------------------------

int LerParametrosHw(void)
{
  //for (unsigned int i = 0; i < sizeof(configHw_st); i++)
  //	*((char*)&configHw + i) = EEPROM.read(ADDR_EEPROM_CONFIGHW + i);
  return readFromEEPROM(ADDR_EEPROM_CONFIGHW, configHw);
}
// ---------------------------------------------------------------------------

boolean VerificarAssinatura()
{
  char tmp[5] = "....";
  readFromEEPROM(ADDR_EEPROM_SIGN, tmp);
  if ( tmp[0] == cfgAssinatura[0] && tmp[1] == cfgAssinatura[1] &&
       tmp[2] == cfgAssinatura[2] && tmp[3] == cfgAssinatura[3] )
    return true;
  else
    return false;
}
//-----------------------------------------------------

boolean GravarAssinatura()
{
  writeToEEPROM(ADDR_EEPROM_SIGN, cfgAssinatura);
}
//-----------------------------------------------------


// serial end ethernet buffer size
#define BUFFER_SIZE 128

WiFiClient cliente;

//------------------------- LOOP --------------------------------

void loop()
{
  uint16_t bytes_read;
  uint8_t net_buf[BUFFER_SIZE] = {0};

  //	if (G_debug != 0) {
  //		Serial.print("Debug: "); Serial.println(G_debug);
  //	}
  // trata requisicoes do server de configuracao:
  serverConfig.handleClient();
//  delay(1);

  // Verifica se um cliente conectou
  cliente = server.available();
//  if (cliente) {
//    cliente.setNoDelay(true);
//    while (!cliente.available()) {
//      //processaKeypad();
//      //Serial.print(".");
//      delay(1);
//    }
//  }

  while (Serial.available() > 0) {
	    // read the incoming byte:
    	int incomingByte = Serial.read();
    	if (cliente && cliente.connected()) {
    		//cliente.print(incomingByte);
    		Serial.write(incomingByte);
    	}
    	delay(20);
  }


  if (cliente && cliente.connected()) {
    // check the network for any bytes to send to the serial
    //Serial.print("!");
    int count = cliente.available();
    //   	Serial.println("");
    //	    Serial.print("!");
    //	    Serial.print(count, DEC);
    if (count > 0) {
      //Serial.print("~");
      bytes_read = cliente.read(net_buf, min(count, BUFFER_SIZE));
      // Para debug de comandos:
      //Serial.print("Cmd: "); Serial.print(bytes_read, DEC); Serial.print(" "); Serial.write(net_buf, bytes_read); Serial.println("");

      Serial.write(net_buf, bytes_read);

      //bool cmdOk = telescope.processaCmd( (char*) net_buf, bytes_read );
      //if (!cmdOk)
      //Serial.println( (char*) net_buf );

      //Serial.flush();
      //delay(20):
    }
    //delay(20);

  }

  delay(50);

  
  while (Serial.available() > 0) {
	    // read the incoming byte:
    	int incomingByte = Serial.read();
    	if (cliente && cliente.connected()) {
    		//cliente.print(incomingByte);
    		Serial.write(incomingByte);
    	}
    	delay(20);
  }

  //Serial.flush();
  delay(10);


  //delay(1);
  delay(50);
}
//--------------------------------------------------------------

void configWiFi(ConfigHw_st *configHw, bool bPrintInfo)
{
  delay(2000);

  IPAddress apIP(10, 0, 0, 1);
  //IPAddress apIP = configHw->netIPAddress;
  IPAddress apSubRede(255,255,255,0);
  //IPAddress apSubRede = configHw->netSubnetMask;
  
  WiFi.softAPConfig(apIP, apIP, apSubRede);

  // Mac Address (para compor o SSID "unico":
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  WiFi.softAPmacAddress(mac);
  IPAddress myIP = WiFi.softAPIP();
  // Anexar os dois ultimos digitos (em HEXA) do mac address ao SSID:
  String macBytes = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
                    String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
  macBytes.toUpperCase();
  String wifiSSIDCompl = String(WiFiSSID) + String("-") + macBytes;
  /* You can remove the password parameter if you want the AP to be open. */
  //WiFi.softAP(wifiSSIDCompl, WiFiSENHA);
  //WiFi.softAP(WiFiSSID);

  WiFi.softAP(wifiSSIDCompl.c_str());

  if (bPrintInfo) {
    Serial.print("SSID: ");
    Serial.println(wifiSSIDCompl.c_str());
    Serial.print("AP endereco IP: ");
    Serial.println(myIP.toString());
    Serial.print("AP mascara de SubRede: ");
    Serial.println(apSubRede.toString());
    Serial.print("Porta: ");
    Serial.println(WiFiPort);
    Serial.print("Porta Configuracao: ");
    Serial.println(WiFiPortConfig);
  }
  server.begin();		// server de interface do telescopio
  server.setNoDelay(true);

  serverConfig.on( "/", HTTP_GET, handleWebRoot );
  serverConfig.on( "/", HTTP_POST, handleWebRootPost );
  serverConfig.on( "/Ceamig-Logo-Foto.jpg", HTTP_GET, handleLogo );
  serverConfig.on( "/setserial", handleSetSerial );
  serverConfig.onNotFound( handleNotFound );
  serverConfig.begin();	// server de configuracao de parametros
}
//--------------------------------------------------------------

void handleWebRoot() {
	String temp;
	char buf[50] = {0};
	
	Serial.println("======= ROOT ========");
	//if (SPIFFS.exists("/CeamigSerialConfig.html")) {
	File f = SPIFFS.open("/CeamigSerialConfig.html", "r");
	temp = f.readString();
	//Serial.println(temp);
	
	// configura os campos da pagina:
	//
	temp.replace("$verFirmware$", VERSAO);
	
	IPAddress myIP = WiFi.softAPIP();
	temp.replace("$enderIP$", myIP.toString());
	
	uint8_t mac[WL_MAC_ADDR_LENGTH];
	WiFi.softAPmacAddress(mac);
	// Anexar os dois ultimos digitos (em HEXA) do mac address ao SSID:
	sprintf(buf, "%02X:%02X:%02X:%02X", mac[WL_MAC_ADDR_LENGTH - 4],
										mac[WL_MAC_ADDR_LENGTH - 3],
										mac[WL_MAC_ADDR_LENGTH - 2],
										mac[WL_MAC_ADDR_LENGTH - 1] ); 
	//macBytes.toUpperCase();
	temp.replace("$macAddr$", buf);  memset(buf, 0, sizeof(buf));
	
	temp.replace("$deviceName$", configHw.deviceName);
	sprintf(buf, "-%02X%02X", mac[WL_MAC_ADDR_LENGTH - 2], mac[WL_MAC_ADDR_LENGTH - 1] );  
	temp.replace("$deviceName_Mac$", buf);   memset(buf, 0, sizeof(buf));

//	switch (configHw.telescopeModel) {
//		case Telescope::MEADE:		temp.replace("Meade_LX200\">", "Meade_LX200\" selected>"); break;
//		case Telescope::CELESTRON:	temp.replace("Celestron_NexStar\">", "Celestron_NexStar\" selected>"); break;
//	}

//	switch (configHw.telescopeType) {
//		case Telescope::ALTAZ:	temp.replace("AltAzimutal\"", "AltAzimutal\" selected"); break;
//		case Telescope::GERMAN:	temp.replace("EquatorialGermanica\"", "EquatorialGermanica\" selected"); break;
//		case Telescope::EQUAT:	temp.replace("EquatorialFork\"", "EquatorialFork\" selected"); break;
//	}

//	temp.replace("$azmRotDireta$", (configHw.inverteAzimute)? "" : "checked");
//	temp.replace("$azmRotInvert$", (configHw.inverteAzimute)? "checked" : "");
//	temp.replace("$azm_steps$", String(configHw.RAStepsPerRev, 0) );
//	temp.replace("$azm_velmax$", String(configHw.slewSpeedAzimute) );
//	temp.replace("$azm_aceler$", String(configHw.aceleracaoAzimute) );

//	temp.replace("$altRotDireta$", (configHw.inverteAltura)? "" : "checked");
//	temp.replace("$altRotInvert$", (configHw.inverteAltura)? "checked" : "");
//	temp.replace("$alt_steps$", String(configHw.DecStepsPerRev, 0) );
//	temp.replace("$alt_velmax$", String(configHw.slewSpeedAltura) );
//	temp.replace("$alt_aceler$", String(configHw.aceleracaoAltura) );

//	temp.replace("$latitude$", String(configHw.latitude, 3) );
//	temp.replace("$longitude$", String(configHw.longitude, 3) );
//	temp.replace("$timezone$", String(configHw.timeZone, 1) );
//	temp.replace("$altitude$", String(configHw.altitude, 0) );

	
	
//	temp.replace("$ender_ip$", configHw.netIPAddress.toString() );
//	temp.replace("$ender_subrede$", configHw.netSubnetMask.toString() );
//	temp.replace("$porta_rede$", String(configHw.netPort) );

	
	serverConfig.send( 200, "text/html", temp.c_str() );
	f.close();
}
//--------------------------------------------------------------

void handleWebRootPost()
{
	
	Serial.println("======= ROOT - POST !!! ========");
	String message = "URI: ";

	message += serverConfig.uri();
	message += "\nArgumentos: ";
	message += serverConfig.args();
	message += "\n";
	for ( uint8_t i = 0; i < serverConfig.args(); i++ ) {
		message += " " + serverConfig.argName( i ) + ": " + serverConfig.arg( i ) + "\n";
	}
	// Arguments: device_name=CeamigWiFi
	// Arguments: Telecopio+Tipo=Meade+LX200&Tipo+Montagem=AltAzimutal&azmRotacao=AZM_DIRETA&
	//   azim_steps=1296000&azm_velmax=1200&azm_aceler=1500&altRotacao=ALT_DIRETA&alt_steps=1294999&alt_velmax=1200&alt_aceler=1500
	// Arguments: latitude=-19&longitude=-43&timezone=-3&altitude=910
	// Arguments: settings_address_adhoc=10.0.0.1&settings_address_adhoc_subnet=255.255.255.0&porta=4030
	for ( uint8_t i = 0; i < serverConfig.args(); i++ ) {
		String nomeArg = serverConfig.argName( i );
		String valArg  = serverConfig.arg( i );
		if ( nomeArg == String("device_name") ) {
			strncpy(configHw.deviceName, valArg.c_str(), 25);
//		} else if ( nomeArg == String("Telecopio+Tipo") ) {
//			if (valArg == String("Celestron_NexStar") ) {
//				configHw.telescopeModel = Telescope::CELESTRON;
//			} else {
//				configHw.telescopeModel = Telescope::MEADE;
//			}
		} else if ( nomeArg == String("ender_ip") ) {
			int parms[4] = {0};
        	short n = parseInt( valArg.c_str(), 4, ".", parms );
        	if ( n == 4 ) {
        		configHw.netIPAddress = IPAddress(parms[0],parms[1],parms[2],parms[3]);
        	}
		} else if ( nomeArg == String("ender_subrede") ) {
			int parms[4] = {0};
        	short n = parseInt( valArg.c_str(), 4, ".", parms );
        	if ( n == 4 ) {
        		configHw.netSubnetMask = IPAddress(parms[0],parms[1],parms[2],parms[3]);
        	}
		} else if ( nomeArg == String("porta_rede") ) {
			configHw.netPort = valArg.toInt();
		}
	}	

	// Gravar na EEPROM:
	GravarParametrosHw();

	handleWebRoot();
	//serverConfig.send( 200 );		// send HTTP status code OK (200)
	
	//Serial.println( message );

}
//--------------------------------------------------------------

void handleSetSerial()
{
  //Serial.println("======= /setserial ========");
  serverConfig.send(204);  // no content!
}
//--------------------------------------------------------------

void handleLogo()
{
  //Serial.println("====  LOGO ====");
  String logo = "/Ceamig-Logo-Foto.jpg";
  File f = SPIFFS.open("/Ceamig-Logo-Foto.jpg", "r");
  serverConfig.streamFile(f, "image/jpeg");   // com problema nessa versao.... :-((
  /*char buf[1024];
    int siz = f.size();
    while(siz > 0) {
  	size_t len = min((int)(sizeof(buf) - 1), siz);
  	f.read((uint8_t *)buf, len);
     	serverConfig.client().write((const char*)buf, len);
  	siz -= len;
    } //- See more at: http://www.esp8266.com/viewtopic.php?p=57334#sthash.3IdCvS1j.dpuf
  */
  f.close();
}
//--------------------------------------------------------------

void handleNotFound() {
  String message = "Arquivo nao encontrado\n\n";
  message += "URI: ";
  message += serverConfig.uri();
  message += "\nMetodo: ";
  message += ( serverConfig.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArgumentos: ";
  message += serverConfig.args();
  message += "\n";

  for ( uint8_t i = 0; i < serverConfig.args(); i++ ) {
    message += " " + serverConfig.argName( i ) + ": " + serverConfig.arg( i ) + "\n";
  }

  serverConfig.send ( 404, "text/plain", message );

  Serial.println( message );
}
//--------------------------------------------------------------
//--------------------------------------------------------------
