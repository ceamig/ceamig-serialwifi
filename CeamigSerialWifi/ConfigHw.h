/****************************************************************************
 * Copyright (C) 2017 by Ceamig - www.ceamig.org.br                         *
 *                                                                          *
 * This file is part of CeamigWiFi.                                         *
 *                                                                          *
 *   CeamigWiFi is free software: you can redistribute it and/or modify it  *
 *   under the terms of the GNU Lesser General Public License as published  *
 *   by the Free Software Foundation, either version 3 of the License, or   *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   CeamigWiFi is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU Lesser General Public License for more details.                    *
 *                                                                          *
 *   See the GNU Lesser General Public at <http://www.gnu.org/licenses/>.   *
 ****************************************************************************/

/**
 * @file ConfigHw.h
 * @author Marcos Ackel
 * @date  June 2021
 * @brief Configuracao de Hardware
 */
#ifndef CONFIGHW_H
#define CONFIGHW_H



// Estrutura de parametros de configuracao:

typedef struct ConfigHw_st {
	// Dispositivo:	
	char deviceName[25+1];
	// Rede:
	IPAddress netIPAddress;
	IPAddress netSubnetMask;
	uint16_t  netPort;
	uint16_t serialBaudRate;
	SerialConfig serialConfig;
};

#endif
