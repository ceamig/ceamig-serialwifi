#include <ESP8266WiFi.h>

// ATENCAO: GPIO0 (FLASH) -> low -> para upload 
//                           high -> boot normal
//          CH_PD -> high

//  Versao minima

// definicoes WiFi:

// Definicoes Pinos I/O
const int LED_PIN = 2;    // ESP-07: LED no GPIO2 - active low (LOW para ligar)

WiFiServer server(80);

void setup() 
{
	initHardware();
	setupWiFi();
	server.begin();
	Serial.println("Server inicializado");
}

void loop() 
{
	// Verifica se um cliente conectou
	WiFiClient cliente = server.available();
	if (!cliente) {
		return;
	}
	
	// Le a primeira linha da requisicao:
	String req = cliente.readStringUntil('\r');
	Serial.println(req);
	cliente.flush();
	
	// Verifica a requisicao:
	int val = -1;   // valor para requisicao invalida
	            
	if (req.indexOf("/led/0") != -1)
		val = 0; // desligar o led
	else if (req.indexOf("/led/1") != -1)
		val = 1; // ligar o led
	else if (req.indexOf("/msg") != -1) //2)
		val = 2; // Mostra mensagem na porta serial
	else if (req.indexOf("/g2/0") != -1)
		digitalWrite( 2, LOW);
	else if (req.indexOf("/g2/1") != -1)
		digitalWrite( 2, HIGH);
	// qualquer outro valor, a requisao e' invalida (-1)
	
	// Ajusta o pino GPIO5 de acordo com a requisicao:
	if (val == 0 || val == 1)
		digitalWrite(LED_PIN, val);
	
	cliente.flush();
	
	// Prepara a resposta em HTML:
	String s = "HTTP/1.1 200 OK\r\n";
	s += "Content-Type: text/html\r\n\r\n";
	s += "<!DOCTYPE HTML>\r\n<html>\r\n";
	
	// Mostra o status do Led, se for o caso:
	if (val == 0 || val == 1) {
		s += "LED -> ";
		s += (val)?"LIGADO":"DESLIGADO";
	} else if (val == 2) { 
		// Mostra mensagem padrao:
		s += "CEAMIG - Servidor em modo AP";
		s += "<br>"; // vai p a prox linha
		s += "Servidor minimo.";
	} else {
		s += "Requisicao Invalida.<br> Tente /led/1, /led/0, or /msg";
	}
	s += "</html>\n";
	
	// Envia a resposta para o cliente (browser)
	cliente.print(s);
	delay(1);
	Serial.println("Cliente desconectado");
	
	// O cliente vai ser realmente desconectado quando a funcao
	// retorna e o objeto 'Cliente' for destruido 
}

void setupWiFi()
{
	WiFi.mode(WIFI_AP);
	
	String AP_NameString = "CEAMIG-1234567890";  // minimo 16 chars (?)
	
	char AP_NameChar[AP_NameString.length() + 1];
	memset(AP_NameChar, AP_NameString.length() + 1, 0);
	
	for (int i=0; i<AP_NameString.length(); i++)
		AP_NameChar[i] = AP_NameString.charAt(i);
	
	WiFi.softAP(AP_NameChar);//, WiFiSENHA);
}

void initHardware()
{
	Serial.begin(74880);
	pinMode(LED_PIN, OUTPUT);
	
	pinMode( 0, OUTPUT);
	pinMode( 2, OUTPUT);
	pinMode( 4, OUTPUT);
	pinMode( 5, OUTPUT);
	pinMode(12, OUTPUT);
	pinMode(13, OUTPUT);
	pinMode(14, OUTPUT);
	pinMode(15, OUTPUT);
	pinMode(16, OUTPUT);
	digitalWrite(LED_PIN, LOW);  //liga o led
	Serial.println("Inicializacao");
}

