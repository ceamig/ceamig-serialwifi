// ESP software serial
// https://github.com/plerup/espsoftwareserial/


#include <SoftwareSerial.h>

// Explicacao:  
// Dx -> (yy)   (yy) e' o numero do GPIO. Ex.: (15) => GPIO15

#ifndef D5
#if defined(ESP8266)
#define D5 (14)
#define D6 (12)
#define D7 (13)
#define D8 (15)
#define TX (1)
#elif defined(ESP32)
#define D5 (18)
#define D6 (19)
#define D7 (23)
#define D8 (5)
#define TX (1)
#endif
#endif



SoftwareSerial testSerial;
#define BAUD_RATE 9600



void setup() 
{
	Serial.begin(9600);

	// .begin(baud, config, rxpin, txpin)
	testSerial.begin(BAUD_RATE, SWSERIAL_8N1, D6, D7);
    
    usbSerial.println(PSTR("\nSoftware serial test started"));

    for (char ch = ' '; ch <= 'z'; ch++) {
        testSerial.write(ch);
    }
    testSerial.println();
}

void loop() {
    while (testSerial.available() > 0) {
        usbSerial.write(testSerial.read());
        yield();
    }
    while (usbSerial.available() > 0) {
        testSerial.write(usbSerial.read());
        yield();
    }
}
